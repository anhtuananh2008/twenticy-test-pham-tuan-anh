<?php 
include('login.php');
include('data.php');
if($_REQUEST['control'] == 'login'){
    $obj = new LoginClass();
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        if(isset($_GET['type']) && $_GET['type'] == 'logout'){
            $obj->logout();
        }else{
            $obj->check_login();
        }
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $obj->login();
    }
}else{
    $obj = new Data;
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $obj->getData();
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $obj->updateData();
    }

    if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
        $obj->delete();
    }
}
?>