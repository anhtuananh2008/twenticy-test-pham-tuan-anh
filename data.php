<?php
class Data
{
    const FILENAME = 'data.json';

    public function __construct(){
        if(!$this->check_login()){
            $res = [
                'error' => true,
                'message' => 'You are not logged in'
            ];
            echo json_encode($res);
            die();
        }
    }

    /**
     * check login
     * return boolean
     */
    public function check_login(){
        if(isset($_SESSION['login'])){
            return true;
        }else{
            return false;
        }
    }

    public function getData(){
        $res = [
            'error' => false,
            'message' => 'success',
            'data' =>  $this->load_file()
        ];
        if(isset($_GET['index'])){
            // get list
            $res['data'] = $this->load_file()[$_GET['index']];
        }else{
            // get posts
            $res['data'] = $this->load_file();
        }
        echo json_encode($res);
    }

    public function updateData(){
        //get param post
        $params = json_decode(file_get_contents('php://input'),true);
        //end
        if(empty($params['title']) || empty($params['description']) || empty($params['content'])){
            echo json_encode([
                'error' => true,
                'message' => 'Please enter full information'
            ]);
            die;
        }
        $data = [
            'title' => $params['title'],
            'description' => $params['description'],
            'content' => $params['content']
        ];
        
        $data_save = $this->load_file();
        if(isset($params['index'])){
            // update
            $data_save[$params['index']] = $data;
        }else{
            // create
            $data_save[] = $data;
        }
        // save file
        file_put_contents(self::FILENAME, json_encode($data_save));
        //
        $res = [
            'error' => false,
            'message' => 'success'
        ];
        echo json_encode($res);
    }

    public function delete(){
        $index = $_GET['index'];
        $data_save_temp = $this->load_file();
        unset($data_save_temp[$index]);
        $data_save = [];
        foreach ($data_save_temp as $key => $value) {
            $data_save[] = $value;
        }
        file_put_contents(self::FILENAME, json_encode($data_save));
        $res = [
            'error' => false,
            'message' => 'success'
        ];
        echo json_encode($res);
    }

    public function load_file(){
        return json_decode(file_get_contents(self::FILENAME),true);
    }
}



?>