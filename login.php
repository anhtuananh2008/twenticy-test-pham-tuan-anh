<?php
// Start the session
session_start();
class LoginClass{
    const USERNAME = 'demo';
    const PASSWORD = 'demo';

    public function login(){
        //get param post
        $params = json_decode(file_get_contents('php://input'),true);
        //end
        // check require 
        if(empty($params['username']) || empty($params['password'])){
            echo json_encode([
                'error' => true,
                'message' => 'Please enter full information'
            ]);
            die;
        }
        //
        // check invalid
        if($params['username'] != self::USERNAME || $params['password'] != self::PASSWORD){
            echo json_encode([
                'error' => true,
                'message' => 'Username or password is wrong'
            ]);
            die;
        }
        //

        // put session
        $_SESSION['login'] = true;

        $res = [
            'error' => false,
            'message' => 'success'
        ];
        echo json_encode($res);
    }

    // destroy session
    public function logout(){
        session_destroy();
    }

    /**
     * check login
     * return boolean
     */
    public function check_login(){
        if(isset($_SESSION['login'])){
            echo 'true';
        }else{
            echo 'false';
        }
    }
}



?>