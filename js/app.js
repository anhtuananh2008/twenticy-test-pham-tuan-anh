const Login = {
    template: '<div style="width:400px;margin:30px auto">\
            <h3 style="text-align:center; margin-bottom:30px">TwentyCi Test</h3>\
            <form method="post" action="login.php">\
                <div class="form-group col-md-12">\
                    <input name="username" placeholder="Username" class="form-control" v-model="frm.username"  v-on:keyup.enter="login"/>\
                </div>\
                <div class="form-group col-md-12">\
                    <input name="password" placeholder="Password" class="form-control" v-model="frm.password"  v-on:keyup.enter="login" />\
                </div>\
                <div class="form-group col-md-12">\
                    <button type="button" class="btn btn-success btn-block" v-on:click="login()">Login</button>\
                </div>\
                <p style="color:red;text-align:center" v-if="message">{{message}}</p>\
            </form>\
        </div>',
    data : function(){
        return {
            message : '',
            frm : {}
        }
    },
    methods :{
        login: function () {
            var self = this;
            axios.post('action.php?control=login',this.frm)
                .then(function (response) {
                    if(!response.data.error){
                        app.$data.isLoggedIn = true;
                        self.$router.push('/list');
                    }else{
                        self.message = response.data.message;
                    }
                })
                .catch(function (error) {
                    self.message = 'Lỗi! Không thể truy cập API. ' + error
                })
        }
    }
}
const List = {
    template: '<<div>\
        <slot></slot>\
        <a class="btn btn-success float-right" href="#/create" style="color:#fff;margin-bottom:10px">Create</a>\
        <p>{{message}}</p>\
        <table class="table table-hover">\
            <thead>\
                <tr>\
                <th>Title</th>\
                <th>Description</th>\
                <th>Content</th>\
                <th colspan="2" style="text-align:center">Action</th>\
                </tr>\
            </thead>\
            <tbody v-if="list_data && list_data.length == 0">\
                <tr><td style="text-align:center" colspan="6">No data</td></tr>\
            </tbody>\
            <tbody v-if="list_data">\
                <tr v-for="(item, index) in list_data" :key="index">\
                <td>{{item.title}}</td>\
                <td>{{item.description}}</td>\
                <td>{{item.content}}</td>\
                <td style="text-align:center"><router-link :to="{ path: `edit/${index}` }">Edit</router-link></td>\
                <td style="text-align:center"><a href="javascript:void(0)" v-on:click="remove(index)">Delete</a></td>\
                </tr>\
            </tbody>\
        </table>\
    </div>',
    data : function(){
        return {
            'list_data' : [],
            'message' : ''
        }
    },
    methods : {
        load : function() {
            var self = this;
            axios.get('action.php?control=data')
                .then(function (response) {
                    if(!response.data.error){
                        self.list_data = response.data.data;
                    }
                })
                .catch(function (error) {
                    self.message = 'Lỗi! Không thể truy cập API. ' + error
                })
        },
        remove : function(index) {
            var self = this;
            axios.delete('action.php?control=data&index='+index)
                .then(function (response) {
                    if(!response.data.error){
                        self.load();
                    }
                })
                .catch(function (error) {
                    self.message = 'Lỗi! Không thể truy cập API. ' + error
                })
        },
    },
    created() {
        this.load();
    }
}
const Edit = {
    template: 
        '<form method="POST" action="action.php">\
            <slot></slot>\
            <p v-bind:style="{color:color}">{{message}}</p>\
            <input type="hidden" v-if="frm.index || frm.index == 0" v-model="frm.index" />\
            <div class="form-group">\
            <label for="title">Title</label>\
            <input type="text" class="form-control" id="title" placeholder="Title" v-model="frm.title">\
            </div>\
            <div class="form-group">\
            <label for="description">Description</label>\
            <textarea class="form-control" id="description" placeholder="Description" v-model="frm.description"></textarea>\
            </div>\
            <div class="form-group">\
            <label for="content">Content</label>\
            <textarea class="form-control" id="content" placeholder="Content" v-model="frm.content"></textarea>\
            </div>\
            <button type="button" v-on:click="submit()" class="btn btn-primary float-right">Submit</button>\
        </form>',
    data : function(){
        return {
            frm : {},
            message : '',
            color : 'red'
        }
    },
    methods : {
        submit : function(){
            var self = this;
            axios.post('action.php?control=data',this.frm)
                .then(function (response) {
                    if(!response.data.error){
                        self.$router.push('/list');
                    }else{
                        self.message = response.data.message;
                    }
                })
                .catch(function (error) {
                    self.message = 'Lỗi! Không thể truy cập API. ' + error
                })
        },
        load : function(index) {
            var self = this;
            axios.get('action.php?control=data&index='+index)
                .then(function (response) {
                    if(!response.data.error){
                        self.frm = response.data.data;
                        self.frm.index = index;
                    }
                })
                .catch(function (error) {
                    self.message = 'Lỗi! Không thể truy cập API. ' + error
                })
        }
    },
    mounted() {
        if(this.$route.params.index){
            this.load(this.$route.params.index);
        }
    }
}

const routes = [{
        path: '/login',
        component: Login
    },
    {
        path: '/list',
        component: List
    },
    {
        path: '/edit/:index',
        component: Edit
    },
    {
        path: '/create',
        component: Edit
    },
    { path: '/', redirect: '/list' }
]

const router = new VueRouter({
    routes // short for `routes: routes`
})

const app = new Vue({
    router,
    data : {
        isLoggedIn : false
    },
    methods: {
        logout: function () {
            var self = this;
            axios.get('action.php?control=login&type=logout')
                    .then(function (response) {
                        self.isLoggedIn = false;
                        self.$router.push('/login');
                    })
                    .catch(function (error) {
                        self.message = 'Lỗi! Không thể truy cập API. ' + error
                    })
        }
    },
    mounted() {
        var self = this;
            axios.get('action.php?control=login')
                    .then(function (response) {
                        if(response.data){
                            self.isLoggedIn = true;
                        }else{
                            self.$router.push('/login');
                        }
                    })
                    .catch(function (error) {
                        self.message = 'Lỗi! Không thể truy cập API. ' + error
                    })
    },
    watch:{
        $route (to, from){
            var self = this;
            if(self.isLoggedIn){
                if(self.$route.path == '/login'){
                    self.$router.push('/list');
                }
            }else{
                self.$router.push('/login');
            }
        }
    } 
    
}).$mount('#app')